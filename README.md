# monocles social for Android

This is the source code for the Android Client of monocles social, a fair social networking platform based on mastodon that tries to protect your privacy. 

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/de.monocles.social/)

